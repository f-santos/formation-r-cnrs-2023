#+TITLE: Formation "Initiation à R", DR 15 CNRS
#+SUBTITLE: Instructions d'installation
#+AUTHOR: Frédéric Santos
#+EMAIL: frederic.santos@u-bordeaux.fr
#+OPTIONS: toc:nil email:t
#+LATEX_HEADER: \usepackage{a4wide}
#+LATEX_HEADER: \usepackage[french]{babel}
#+LATEX_HEADER: \usepackage{mathpazo}
#+LATEX_HEADER: \usepackage{mdframed}
#+LATEX_HEADER: \usepackage{minted}
#+LATEX_HEADER: \usepackage[usenames,dvipsnames]{xcolor} % For colors with friendly names
#+LATEX_HEADER: \usemintedstyle{friendly} % set style if needed, see https://frama.link/jfRr8Lpj
#+LATEX_HEADER: \mdfdefinestyle{mystyle}{linecolor=gray!30,backgroundcolor=gray!30}
#+LATEX_HEADER: \BeforeBeginEnvironment{minted}{%
#+LATEX_HEADER: \small \begin{mdframed}[style=mystyle]}
#+LATEX_HEADER: \AfterEndEnvironment{minted}{%
#+LATEX_HEADER: \end{mdframed} \medskip \normalsize}
#+LATEX_HEADER: \usepackage{float}
#+LATEX_HEADER: \usepackage{url}
#+LATEX_HEADER: %% Formatting of verbatim outputs (i.e., outputs of R results):
#+LATEX_HEADER: \DefineVerbatimEnvironment{verbatim}{Verbatim}{%
#+LATEX_HEADER:   fontsize = \small,
#+LATEX_HEADER:   frame = leftline,
#+LATEX_HEADER:   formatcom = {\color{gray!97}}
#+LATEX_HEADER: }
#+LATEX_HEADER: \usepackage{titlesec}
#+LATEX_HEADER: \titlelabel{\thetitle.\quad}
#+LANGUAGE: fr

** Installation d'un tableur
Si aucun tableur n'est déjà présent sur votre poste, [[https://fr.libreoffice.org/download/telecharger-libreoffice/][LibreOffice Calc]], tableur libre et gratuit, sera un excellent choix.

** Installation d'un éditeur de texte
Nous aurons également besoin d'un éditeur de texte avancé. Si vous n'en avez pas encore, [[https://atom.io/][Atom]] ou [[https://www.sublimetext.com/][Sublime Text]] sont de bons choix, disponibles pour tous les systèmes d'exploitation (une très brève démonstration utilisant Sublime Text sera faite au début de la formation).

** Installation de R
*** Pour R lui-même
R est disponible pour tous les systèmes d'exploitation :
- *Linux* : des instructions pour les distributions les plus courantes peuvent être trouvées [[https://cran.r-project.org/bin/linux/][sur le site officiel]] ;
- *Mac OS* : des instructions détaillées sont là aussi disponibles [[https://cran.r-project.org/bin/macosx/][sur le site officiel]] ; notez qu'il sera également nécessaire d'installer [[https://www.xquartz.org/][XQuartz]] ;
- *Windows* : installer successivement [[https://cran.r-project.org/bin/windows/base/][le logiciel R]], puis [[https://cran.r-project.org/bin/windows/Rtools/][Rtools]].

Pour la formation, nous aurons besoin d'une version /récente/ de R (version 4.2.0 ou plus récente) : si vous avez déjà une version de R plus ancienne sur votre poste, il serait préférable de la désinstaller et d'installer la dernière version à la place.

*** Installation de packages R
Installer des /packages additionnels/ de R sera également nécessaire au cas où nous n'aurions pas une bonne connexion internet sur place. Pour ce faire, il suffit de copier-coller (en une seule fois) tout le contenu du bloc suivant dans une console R, puis de valider par la touche Entrée. Au préalable, assurez-vous d'être connecté à Internet.

#+begin_src R :exports code :eval no
install.packages(c("car", "corrplot", "lattice", "rmarkdown", "tidyverse", "vegan"),
                 dep = TRUE, repos = "https://cran.wu.ac.at/")
#+end_src

Attention, l'installation peut prendre un certain temps.

** Installation de Rstudio
Pour terminer, il est nécessaire d'installer l'IDE [[https://posit.co/download/rstudio-desktop/][Rstudio]], qui sera l'interface utilisée pendant la formation.
